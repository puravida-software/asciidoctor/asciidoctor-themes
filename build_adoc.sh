./gradlew clean build publishToMavenLocal && \
./gradlew -b asciidoctor.gradle asciidoctor -PpuravidaVersion=$(./gradlew properties -q | grep "version:" | awk '{print $2}')
