package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ThemesTests {

    @Test
    public void rocketPandaIsApplied() {

        Map<String,Object> attributes = new HashMap<>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setAttributes(attributes);

        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":toc: left\n" +
                        ":theme: rocket-panda\n" +
                        ":toc-collapsable: \n" +
                        "== Hola \n"+
                        "caracola \n"+
                        "== Hello \n"+
                        "caracol \n"+
                        "=== Hi \n"+
                        "caracol \n"+
                        ""), options);
        assertThat(converted).contains("rocket-panda");
    }


}
